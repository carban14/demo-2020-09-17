# 静态博客入门到精通

![ad](./images/upyun.png)

## 基础内容

* 自我介绍
  * 心路历程
  * 为什么要写博客
* 静态博客
  * 对比
  * 优势
* Markdown
  * 简单入门
  * 编辑器
* Hugo
  * [简介](https://gohugo.io/about/what-is-hugo/)
  * [官网安装教程](https://gohugo.io/getting-started/quick-start/)
  * 搭建博客
  * 第一篇文章
* Gitlab CI
  * 配置
  * 发布到 Gitlab pages
  * 配置私有域名
* 云主机上搭建
  * 使用 Caddy 或者 Nginx 部署你的静态页面
* 使用又拍云 CDN 加速静态资源
  * ![加入又拍云联盟领取奖励](https://www.upyun.com/static/img/%E6%A0%B7%E5%BC%8F%E5%9B%BE.7cf927c.png)

## 扩展内容

* 设计一个静态页面生成工具
  * 原理
  * 需求
  * 模块设计
  * 演示效果

## 总结

* 折腾这些有没有用？
* 是否需要重复造轮子？
* 重点在于内容

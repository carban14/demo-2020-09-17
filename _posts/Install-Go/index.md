# Go

```bash
# 下载二进制包
wget https://golang.google.cn/dl/go1.15.2.linux-amd64.tar.gz

# 解压到系统目录
sudo tar -C /usr/local -xzf go1.15.2.linux-amd64.tar.gz

# 配置环境变量
cat <<EOF >> ~/.profile
export GOPATH=\$HOME/go
export PATH=\$PATH:/usr/local/go/bin:\$HOME/go/bin
EOF

# 使改动立即生效
source ~/.profile

# 检查是否安装成功
go env
```

可以参考官方教程  [https://golang.google.cn/doc/install](https://golang.google.cn/doc/install)
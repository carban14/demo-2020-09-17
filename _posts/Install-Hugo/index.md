# Hugo

使用 go get 方式安装

```bash
# 配置 go proxy 加快下载速度
export GOPROXY=https://goproxy.cn,direct

# 启用 go mod 模式
export GO111MODULE=on

# 拉取目前最新稳定版本
go get github.com/gohugoio/hugo@v0.75.1

# 检查 hugo 安装是否成功
hugo version
```

依然可以参考官方教程 [https://gohugo.io/getting-started/installing](https://gohugo.io/getting-started/installing)
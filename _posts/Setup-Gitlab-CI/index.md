
创建一个 Gitlab 账号，略。

blog 目录下添加一个 `.gitlab-ci.yml` 文件

参考 [https://gitlab.com/pages/hugo/-/blob/master/.gitlab-ci.yml](https://gitlab.com/pages/hugo/-/blob/master/.gitlab-ci.yml)

修改配置文件中 `vim config.yml`

baseURL = "[https://carban14.gitlab.io](https://carban14.gitlab.io/)"

假设我用户名是 carban14

新建一个项目 carban14.gitlab.io

设置中打开 Settings - General - Visibility, project features, permission - Pages - Everyone

访问 [https://carban14.gitlab.io/](https://carban14.gitlab.io/) 即可看到页面

别忘了，需要发布的文章顶部将  draft 置为 true 。

更多参考 [https://docs.gitlab.com/ee/user/project/pages/](https://docs.gitlab.com/ee/user/project/pages/)

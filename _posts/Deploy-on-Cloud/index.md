# 云主机上部署

## Nginx 配置

```sh
sudo apt install nginx -y

cat <<EOF >> /etc/nginx/sites-enabled/default
server {
        listen 80;
        listen [::]:80;

        server_name blog.carban14.xyz;

        root /home/shank/demo/_pub;
        index index.html;

        location / {
                try_files $uri $uri/ =404;
        }
}
EOF

```

## Github Action

deploy.yml

```yml
name: Deploy

on:
  push:
    branches: [ master ]

jobs:
  hugo:
    name: Hugo
    runs-on: ubuntu-latest
    steps:
    - name: Compile content on the server
      uses: appleboy/ssh-action@master
      with:
        host: ${{ secrets.HOST }}
        username: ${{ secrets.USERNAME }}
        password: ${{ secrets.PASSWORD }}
        script_stop: true
        script: |
          cd blog.carban14.xyz
          git pull
          hugo --destination=./public
```
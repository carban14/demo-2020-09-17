# 直播演示用材料

可以用直播介绍的静态博客生成工具转换为静态网页。

```sh
# 配置 GOPROXY 加速下载，要求 Go > 1.12
export GOPROXY=https://goproxy.cn,direct
export GO111MODULE=on

# 获取静态博客生成工具
go get -u -v gitlab.com/carban14/blog-generator

# 克隆本仓库
git clone https://gitlab.com/carban14/demo-2020-09-17.git

# export PATH=$PATH:`go env GOPATH`/bin
cd demo-2020-09-17
blog-generator -c config.yaml

# 结果生成在 _pub 目录，本地起一个 web 服务验证
python3 -m http.server -d _pub 8088

# open 127.0.0.1:8088
```